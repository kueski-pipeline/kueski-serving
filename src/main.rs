mod classifier_model;
mod entities;
mod service;
mod persistence;
mod command_line;

use std::sync::Arc;

use service::make_service;

use crate::persistence::PersistenceClient;
use crate::{classifier_model::ClassifierModel};

#[actix_web::main]
async fn main() -> anyhow::Result<()> {
    let args = command_line::CommandLine::get_args();
    let model = ClassifierModel::load_from_file(&args.model_path)?;
    let persistence_client = PersistenceClient::new(&args.connection_string).await?;
    
    let model_arc = Arc::new(model);
    let persistence_client_arc= Arc::new(persistence_client);

    make_service(&args.host, args.port, model_arc, persistence_client_arc).await?;

    Ok(())
}
