use std::sync::Arc;

use actix_web::{get, web, HttpResponse};

use crate::{classifier_model::ClassifierModel, persistence::PersistenceClient};


#[get("/health")]
pub async fn health() -> HttpResponse {
  HttpResponse::NoContent()
		.finish()
}

#[get("/predict/{client_id}")]
pub async fn predict(client_id: web::Path<String>, model: web::Data<Arc<ClassifierModel>>, persistence: web::Data<Arc<PersistenceClient>>) -> HttpResponse {
  
  let client_data_result = persistence.get(&client_id).await;
  let client_data = match client_data_result {
    Ok(database_result) => {
      match database_result {
        Some(client_data) => client_data,
        None => return HttpResponse::NotFound().finish()
      }
    },
    Err(_) => {
      return HttpResponse::InternalServerError().body("Error while loading from database")
    }
  };

  let inference_result = model.infer(client_data);
  match inference_result {
    Ok(inference_result) => HttpResponse::Ok().json(&inference_result),
    Err(_) => HttpResponse::InternalServerError().body("Error while running inference")
  }  
}

#[get("/client/{client_id}")]
pub async fn get_client(client_id: web::Path<String>, persistence: web::Data<Arc<PersistenceClient>>) -> HttpResponse {
  let client_data_result = persistence.get(&client_id).await;

  match client_data_result {
    Ok(database_result) => {
      match database_result {
        Some(client_data) => {
          HttpResponse::Ok().json(&client_data)
        },
        None => return HttpResponse::NotFound().finish()
      }
    },
    Err(_) => {
      return HttpResponse::InternalServerError().body("Error while loading from database")
    }
  }
}