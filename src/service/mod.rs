mod routes;
use std::sync::Arc;
use actix_web::{HttpServer, App, web};

use crate::{classifier_model::ClassifierModel, persistence::PersistenceClient};

pub async fn make_service(host: &str, port: u16, model: Arc<ClassifierModel>, persistence: Arc<PersistenceClient>) -> anyhow::Result<()> {
	HttpServer::new(move || {
		App::new()
			.app_data(web::Data::new(model.clone()))
			.app_data(web::Data::new(persistence.clone()))
			.service(routes::health)
			.service(routes::predict)
			.service(routes::get_client)
	})
	.bind((host, port))?
	.run()
	.await?;

	Ok(())
}