use tokio_postgres::{Client, NoTls};

use crate::entities::inference_input::InferenceInput;

pub struct PersistenceClient {
	client: Client,
}

impl PersistenceClient {

	pub async fn new(connection_string: &str) -> anyhow::Result<PersistenceClient> {
		let (client, _) = tokio_postgres::connect(connection_string, NoTls).await?;
		
		let persistence_client = PersistenceClient {
			client
		};

		Ok(persistence_client)
	}

	pub async fn get(&self, client_id: &str) -> anyhow::Result<Option<InferenceInput>> {
		let query = "
			SELECT
				avg_amount_loans_previous,
				nb_previous_loans,
				years_on_the_job,
				flag_own_car,
				age
			FROM
				client_data.features
			WHERE
				features.id = $1";
		
		match self.client.query(query, &[&client_id.to_string()]).await?.first() {
			Some(row) => {
				let avg_amount_loans_previous = row.get("avg_amount_loans_previous");
				let nb_previous_loans = row.get("nb_previous_loans");
				let years_on_the_job = row.get("years_on_the_job");
				let flag_own_car = row.get("flag_own_car");
				let age = row.get("age");		
				
				let client_data = InferenceInput::new(nb_previous_loans, avg_amount_loans_previous, age, years_on_the_job, flag_own_car);

				Ok(Some(client_data))
			},
			None => Ok(None)
		}
	}
}