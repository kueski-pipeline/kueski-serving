use clap::Parser;

#[derive(Parser, Debug)]
#[clap(author="André Claudino", version, about = "An API for serving kueski credit risk model and its features", long_about = None)]
pub struct CommandLine {
	
	#[clap(short, long)]
	pub model_path: String,

	#[clap(short, long)]
	pub connection_string: String,	

	#[clap(long)]
	pub host: String,

	#[clap(long)]
	pub port: u16,
}

impl CommandLine {
	pub fn get_args() -> CommandLine {
		CommandLine::parse()
	}
}