use std::{fmt::{Debug}};
use tract_onnx::prelude::ToDim;
use tract_onnx::prelude::{tract_ndarray, TypedOp, TypedFact, SimplePlan};

use tract_onnx::{prelude::{Framework, InferenceModelExt, InferenceFact, Datum, tvec}, tract_hir::shapefactoid};

use crate::entities::{inference_output::InferenceOutput, inference_input::InferenceInput};

type RunnableGraph = SimplePlan<TypedFact, Box<dyn TypedOp>, tract_onnx::prelude::Graph<TypedFact, Box<dyn TypedOp>>>;

#[derive(Debug, Clone)]
pub struct ClassifierModel {
	runnable_graph: RunnableGraph
}

impl ClassifierModel {

	pub fn load_from_file(model_path: &str) -> anyhow::Result<Self> {
		let features = InferenceFact::dt_shape(f32::datum_type(), shapefactoid![1_i32, 5_i32]);
		
		let runnable_graph = tract_onnx::onnx()
        .model_for_path(model_path)?
        .with_input_fact(0, features)?
        .into_optimized()?
        .into_runnable()?;
		
		let classifier_model = Self {runnable_graph};

		Ok(classifier_model)
	}

	pub fn infer(&self, client_metadata: InferenceInput) -> anyhow::Result<InferenceOutput> {
		let input_data = client_metadata.to_inference_vector();
    let inputs = tract_ndarray::Array::from_shape_vec((1,5), input_data)?.into();
    let outputs = self.runnable_graph.run(tvec!(inputs))?;

		let prediction = &outputs[0];
		println!("{prediction:?}");
		let probabilitites1 = &outputs[1];
		
		// InferenceOutput::new(probabilitites0);
		InferenceOutput::new(probabilitites1)
	}
}