use serde::Serialize;

#[derive(Serialize, Debug)]
pub struct InferenceInput {
	nb_previous_loans: u32,
	avg_amount_loans_previous: f32,
	age: i32,
	years_on_the_job: i32,
	flag_own_car: u32,
}

impl InferenceInput {

	pub fn new(nb_previous_loans: u32, avg_amount_loans_previous: f32, age: i32, years_on_the_job: i32, flag_own_car: u32,) -> Self {
		Self { nb_previous_loans, avg_amount_loans_previous, age, years_on_the_job, flag_own_car }
	}

	pub fn to_inference_vector(&self) -> Vec<f32> {
		vec![
			self.age as f32,
			self.years_on_the_job as f32,
			self.nb_previous_loans as f32,
			self.avg_amount_loans_previous as f32,
			self.flag_own_car as f32
		]
	}
}