use std::sync::Arc;

use serde::Serialize;
use tract_onnx::{prelude::Tensor};

#[derive(Serialize, Debug)]
pub struct InferenceOutput {
	prediction: usize,
	probabilities: Probability,
}

#[derive(Serialize, Debug)]
pub struct Probability {
	class0: f32,
	class1: f32
}

impl InferenceOutput {
	pub fn new(probabilities: &Arc<Tensor>) -> anyhow::Result<Self> {
		let best_with_score  = probabilities
			.to_array_view::<f32>()?
			.iter()
			.cloned()
			.enumerate()
			.map(|(prediction, probability)|{
				println!("prediction {prediction:?}");
				println!("probability {probability:?}");

				(prediction, probability)
			})
			.max_by(|(_,a), (_,b)| a.partial_cmp(&b).unwrap())
			.map(|(prediction, probability)|{

				let probabilities =
					if prediction == 0 && probability <= 0.5 {
						Probability{ class0: probability, class1: 1.0-probability }
					} else {
						Probability{ class0: 1.0-probability, class1: probability }
					};
				
				InferenceOutput {
					prediction,
					probabilities
				}
			}).unwrap();

		Ok(best_with_score)
	}
}