# Kueski Serving


```
kueski-serving 0.1.0
André Claudino
An API for serving kueski credit risk model and its features

USAGE:
    kueski-serving --model-path <MODEL_PATH> --connection-string <CONNECTION_STRING> --host <HOST> --port <PORT>

OPTIONS:
    -c, --connection-string <CONNECTION_STRING>    
    -h, --host <HOST>                              
        --help                                     Print help information
    -m, --model-path <MODEL_PATH>                  
    -p, --port <PORT>                              
    -V, --version                                  Print version information
```