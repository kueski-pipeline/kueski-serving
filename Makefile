DOCKER_IMAGE_NAME=kueski.com/kueski-srving
DOCKER_IMAGE_TAG=1.0.0

docker/kueski-serving:
	cargo build --release
	cp target/release/kueski-serving docker/kueski-serving

docker/image: docker/kueski-serving
	docker build docker -f docker/Dockerfile -t $(DOCKER_IMAGE_NAME):$(DOCKER_IMAGE_TAG)
	touch docker/image

docker/push: docker/image
	docker push $(DOCKER_IMAGE_NAME):$(DOCKER_IMAGE_TAG)
	touch docker/push

docker/push-latest: docker/image docker/login
	docker tag $(DOCKER_IMAGE_NAME):$(DOCKER_IMAGE_TAG) $(DOCKER_IMAGE_NAME):latest
	docker push $(DOCKER_IMAGE_NAME):latest
	touch docker/push-latest

clean:
	rm -rf docker/package
	rm -rf docker/image
	rm -rf docker/push
	rm -rf docker/push-latest
	rm -rf docker/login
	rm -rf install
